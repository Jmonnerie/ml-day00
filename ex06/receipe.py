# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    receipe.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/05/26 02:22:35 by jojomoon          #+#    #+#              #
#    Updated: 2020/05/26 12:15:21 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import os
from simple_term_menu import TerminalMenu


cookbook = {
    'sandwich': {
        'ingredients': ['ham', 'bread', 'cheese', 'tomatoes'],
        'meal': 'lunch',
        'prep_time': 10
    },
    'cake': {
        'ingredients': ['flour', 'sugar', 'eggs'],
        'meal': 'dessert',
        'prep_time': 60
    },
    'salad': {
        'ingredients': ['avocado', 'arugula', 'tomatoes', 'spinach'],
        'meal': 'lunch',
        'prep_time': 15,
    }
}


def print_receipe(recipe_name):
    recipe = cookbook.get(recipe_name)
    if recipe:
        print(f'\nRecipe for {recipe_name}:')
        print(f'Ingredients list: {recipe["ingredients"]}')
        print(f'To be eaten for {recipe["meal"]}')
        print(f'Takes {recipe["prep_time"]} minutes of cooking.\n')
    else:
        print("\nError: Unknown recipe.\n")


def delete(receipe_name):
    if receipe_name in cookbook:
        del cookbook[receipe_name]
        print(f'\n{receipe_name.capitalize()} recipe deleted.\n')

    else:
        print("\nError: Unknown recipe.")


def add(recipe_name, ingredients, meal, prep_time):
    new_recipe = {
        'ingredients': ingredients,
        'meal': meal,
        'prep_time': prep_time,
    }
    try:
        cookbook[recipe_name] = new_recipe
        print(f"New receipe {recipe_name} added successfully")
    except KeyError:
        print(f"Error: '{recipe_name}' already exists")


def print_all_receipe():
    if len(cookbook) == 0:
        print("No existant receipes")
        return
    for key in cookbook:
        print_receipe(key)


def add_input():
    recipe_name = ""
    while recipe_name == "":
        recipe_name = input("\nRecipe name:\n>> ").strip()
        if recipe_name == "":
            os.system('clear')
            print("\nError: The recipe name can't be empty.")
        if recipe_name in cookbook:
            os.system('clear')
            print(f"\nError: A {recipe_name} recipe already exists.")
            recipe_name = ""

    os.system('clear')
    ingredients = []
    while len(ingredients) < 1:
        ingredients = list(filter(
            None,
            input("\nIngredients (separated by comas):\n>> ")
            .replace(' ', '')
            .split(',')
        ))
        if len(ingredients) < 1:
            print("\nError: The ingredients list can't be empty.")
        if len(ingredients) != len(set(ingredients)):
            print("\nError: Duplicates in ingredients list.")
            ingredients = []

    meal = ""
    while meal == "":
        meal = input("\nMeal type:\n>> ").strip()
        if meal == "":
            print("\nError: The meal type can't be empty.")

    prep_time = 0
    while prep_time == 0:
        time = input("\nPreparation time in minutes:\n>> ")
        try:
            prep_time = int(time)
            if prep_time <= 0:
                print("\nError: Preparation time must be at least 1 minute.")
        except ValueError:
            print("\nError: Enter a valid integer.")

    add(recipe_name, ingredients, meal, prep_time)


def main():
    main_menu_items = ["Add a receipe",
                       "Delete a receipe",
                       "Print a receipe",
                       "Print the cookbook",
                       "Quit"]
    main_menu_cursor = "> "
    main_menu_cursor_style = ("fg_red", "bold")
    main_menu_style = ("bg_red", "fg_yellow")
    main_menu_exit = False

    main_menu = TerminalMenu(menu_entries=main_menu_items,
                             menu_cursor=main_menu_cursor,
                             menu_cursor_style=main_menu_cursor_style,
                             menu_highlight_style=main_menu_style)

    while not main_menu_exit:
        os.system('clear')
        main_selected = main_menu.show()

        if main_selected == 0:
            add_input()
        elif main_selected == 1:
            recipe_name = input(
                "\nPlease enter the recipe's name to delete:\n>> ")
            delete(recipe_name)
        elif main_selected == 2:
            recipe_name = input(
                "\nPlease enter the recipe's name to print:\n>> ")
            print_receipe(recipe_name)
        elif main_selected == 3:
            print_all_receipe()
        elif main_selected == 4:
            print("Goodbye !")
            main_menu_exit = True
        input("Press enter to continue\n")


if __name__ == "__main__":
    main()
