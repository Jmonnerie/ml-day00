# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    count.py                                           :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/25 15:25:45 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/25 15:26:20 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import sys
import string


def text_analyser(*args):
    """\n    This function counts the number of upper characters, lower characters, 
    punctuation and spaces in a given text."""
    userIn = ""
    upper = 0
    lower = 0
    punct = 0
    space = 0
    total = 0
    # If too much params
    if len(args) > 1:
        return print("ERROR")

    # Get the good phrase to compute
    if len(args) == 0:
        userIn = input("What is the text to analyse?\n")
    else:
        userIn = args[0]

    # Increment good counters
    for char in userIn:
        if char.islower():
            lower += 1
        elif char.isupper():
            upper += 1
        elif char == ' ':
            space += 1
        # I know that many signs in string.punctuation are not
        # "usual punctuation" but in python, "punctuation characters"
        # are represented in string.punctuation :p
        elif char in string.punctuation:
            punct += 1
        else:
            total += 1

    # Print
    print("The text contains %d characters:\n" %
          (upper + total + lower + punct + space))
    print("- %d upper letters\n" % upper)
    print("- %d lower letters\n" % lower)
    print("- %d punctuation marks\n" % punct)
    print("- %d spaces" % space)


def main():
    text_analyser(*(sys.argv[1:]))


if __name__ == "__main__":
    main()
