# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    whois.py                                           :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/25 15:10:56 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/25 15:25:00 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import sys
import math


def is_real_number(s):
    try:
        num = int(s)
    except ValueError:
        return False
    if math.isnan(num) or math.isinf(num):
        return False
    return True


def main():
    # If nb of params are not good
    if len(sys.argv) != 2:
        if len(sys.argv) == 1:
            sys.exit(1)
        sys.exit("ERROR")
    number = sys.argv[1]
    # If it's not a number
    if not is_real_number(number):
        sys.exit("ERROR")
    # If it's a zero
    if int(number) == 0:
        print("I'm Zero.")
    # If he is Even
    elif int(number) % 2 == 0:
        print("I'm Even.")
    # If he is Odd
    else:
        print("I'm Odd.")


if __name__ == "__main__":
    main()
