# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    operations.py                                      :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/25 17:45:55 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/26 00:59:53 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import sys


class UsageError(Exception):
    pass


# Calc function(v1, v2) => return 5 values
def operations(i, j):
    add = i + j
    sub = i - j
    mult = i * j
    try:
        div = i / j
        mod = i % j
    except ZeroDivisionError:
        div = "ERROR (div by zero)"
        mod = "ERROR (modulo by zero)"
    return (add, sub, mult, div, mod)


# Verify params
def main():
    if len(sys.argv) < 3:
        raise UsageError()
    elif len(sys.argv) > 3:
        print("InputError: too many arguments\n")
        raise UsageError()
    else:
        try:
            i = int(sys.argv[1])
            j = int(sys.argv[2])
            print(("Sum:        {}\n" +
                   "Difference: {}\n" +
                   "Product:    {}\n" +
                   "Quotient:   {}\n" +
                   "Remainder:  {}\n").format(*operations(i, j)))
        except ValueError:
            print("InputError: only numbers\n")
            raise UsageError()


if __name__ == "__main__":
    try:
        main()
    except UsageError:
        print("Usage: python operations.py <number1> <number2>\n" +
              "Example\n" +
              "python operations.py 10 3\n")
