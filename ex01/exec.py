# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    exec.py                                            :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/25 14:27:13 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/25 15:01:55 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import sys


def main():
    # Concat all params
    string = ""
    for param in sys.argv[1:]:
        string += ' ' + param
    # Deleting first space (Better than other testing way. Ask me why!)
    string = string[1:]
    # Iterate in reversed string
    for char in string[::-1]:
        # Print the good char according to subject
        if char.isupper():
            print(char.lower(), end='')
        elif char.islower():
            print(char.upper(), end='')
        else:
            print(char, end='')


if __name__ == "__main__":
    main()
