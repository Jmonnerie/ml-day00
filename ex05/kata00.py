# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    kata00.py                                          :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/26 01:48:04 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/26 01:48:04 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

t = (19, 42, 21)


def main():     # TODO in one print
    print(f'The three numbers are: {t[0]}', end='')
    for e in t[1:]:
        print(', ' + str(e), end='')


if __name__ == "__main__":
    main()
