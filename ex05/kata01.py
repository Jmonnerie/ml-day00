# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    kata01.py                                          :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/26 02:01:54 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/26 02:01:54 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

from string import Template

languages = {
    'Python': 'Guido van Rossum',
    'Ruby': 'Yukihiro Matsumoto',
    'PHP': 'Rasmus Lerdorf',
}

template = Template("$language was created by $creator")


def main():
    for language, creator in languages.items():
        print(template.substitute(language=language, creator=creator))


if __name__ == "__main__":
    main()
