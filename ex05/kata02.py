# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    kata02.py                                          :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/26 02:04:46 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/26 02:04:46 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import datetime
t = (3, 30, 2019, 9, 25)


def main():
    date = datetime.datetime(
        year=t[2],
        month=t[3],
        day=t[4],
        hour=t[0],
        minute=t[1]
    )
    print(date.strftime("%m/%d/%Y %H:%M"))


if __name__ == "__main__":
    main()
